import { Component } from '@angular/core';

declare var liff: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'line-liff';
  idToken: any = '';
  displayName = '';
  pictureUrl: any = '';
  statusMessage: any = '';
  userId = '';

  ngOnInit(): void {
    this.initLine();
  }

  initLine(): void {
    liff.init({ liffId: '1656089558-qE3aKnj0' }, () => {
      if (liff.isLoggedIn()) {
        this.runApp();
      } else {
        liff.login();
      }
    });
  }

  async runApp() {
    const idToken = liff.getIDToken();
    const profile = await liff.getProfile()
    console.log('idToken', idToken);
    console.log('profile', profile);
    this.idToken = idToken;
    this.displayName = profile.displayName;
    this.pictureUrl = profile.pictureUrl;
    this.statusMessage = profile.statusMessage;
    this.userId = profile.userId;
    // liff.getProfile().then(profile => {
    //   console.log(profile);
    //   this.displayName = profile.displayName;
    //   this.pictureUrl = profile.pictureUrl;
    //   this.statusMessage = profile.statusMessage;
    //   this.userId = profile.userId;
    // }).catch(err => console.error(err));
  }

  logout(): void {
    liff.logout();
    window.location.reload();
  }
}
